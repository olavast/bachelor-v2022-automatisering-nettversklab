# Link til git
https://gitlab.stud.idi.ntnu.no/olavast/bachelor-v2022-automatisering-nettversklab

# Manual
Manualen inneholder informasjon om pakkene som skal installeres og forberedelsene som gjøres i forkant.

Nødvendigheter for installasjon: Datamaskin med en Linux-distribusjon installert.

## Førstegangsinstallasjon
Før du kan komme i gang og og administrere nettverksutstyr i Cisco-laboratoriet* må Ansible, TFTP og andre avhengigheter installeres på en maskin med et Linux OS. Merk: Ubuntu-distribusjonen er anbefalt da prototype-utviklingen er basert på denne. Testmiljøet kan installeres på eksempelsvis (en datamaskin med/uten liveboot fra USB, en VM eller Raspberry Pi). En forutsetning for at løsningen skal fungere er at testmiljøet må være på samme lokalnett som nettverksutstyret.


### Scapy
Scapy er et Python-bibliotek som er nyttig som et verktøy for nettverksadministrering. Her benyttes verktøyet for å  oppdage nettverksenheter på nettverket, og som resultat vil verktøyet skrive ut alle aktive IP-adresser og tilknyttede MAC-adresser.

For å installere Scapy-pakkebiblioteket skriv i kommandolinjen

> sudo apt install python3-scapy


### Ansible
For mer informasjon om installasjonsprosessen se [her](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).

1. Installasjon av Ansible
> sudo add-apt-repository --yes --update ppa:ansible/ansible

> sudo apt install ansible

2. Denne må lastes ned for at Ansible skal kunne kjøre Python-skripter
> sudo apt install python-is-python3


3. *Ansible galaxy collection* for å lettere konfigurere Cisco-utstyret
> ansible-galaxy collection install cisco.ios


### TFTP-server

1. Last ned TFTP-serveren
> sudo apt install tftpd-hpa
2. Konfigurer TFTP-serveren.
Åpne filen */etc/default/tftpd-hpa* som burde se sånn ut:


`# /etc/default/tftpd-hpa`<br /><br />
`TFTP_USERNAME="tftp"`<br />
`TFTP_DIRECTORY="/srv/tftp"`<br />
`TFTP_ADDRESS=":69"`<br />
`TFTP_OPTIONS="--secure"`

Her går det an å bytte ut innholdet i *TFTP_DIRECTORY* til TFTP-mappen i Git. Gjøres dette må eierskapet av mappen byttes til TFTP gruppen og brukeren og så restarte TFTP-serveren. Dette gjøres med disse kommandoene:

Kjøres i mappen TFTP-serveren bruker
> sudo chown -R tftp:tftp .

> sudo systemctl restart tftpd-hpa

Du kan også benytte mappestrukturen */srv/tftp*, men da må må filene i TFTP-mappen i Git bli overført til mappen som har valgt for TFTP-serveren.



## Fremgangsmåte 
Før du begynner sørg for at TFTP-serveren kjører.

1. Skru av strømmen på alle podene som skal oppdateres eller bli konfigurert
2. Koble til en ethernetkabel fra g0/0 for 2901, g0/0/0 for 4221, på 3650 g0/0/1, på 2960 og 2960 plus g0/1  til svitsjen på toppen av poddene. (Det er viktig å bruke rett ethernet port på nettverksutstyret for at oppdateringsscriptet skal fungere)
3. Skru på strømmen igjen og vent til alt utstyret har bootet opp og satt opp SSH. Dette kan ta opp til 10 minutter

Nå er nettverksutstyret klart for å bli konfigurert av Ansible. Det er laget flere *playbooks* for diverse ulike formål som bli forklart under. Den letteste måten å gjøre dette er ved å bruke bash-filen *script.sh*, som blir brukt på følgene måte:

> ./kjor.sh playbooks/\<navn på playbook>

Her er en liste over de nåverende playbooks:
### vis-versjon
vis versjon er en veldig enkel *playbook* som bare vil sende tilbake hvilken IOS versjon nettverksutstyret kjører på, hvilken type det er og hvilken oppstarts-image den benytter seg av.

### opprydning
Denne *playbooken* vil slette oppstartskonfigurasjonen og restarte ruteren på nettverksutstyret.

### oppgrader-*
Brukes til å oppdatere nettverksutstyret, for ruterne og L2-svitsjer må disse fylles inn. 

`old_image_name: *`<br /> Fyll inn navnet på den gamle .BIN-filen <br />
`new_image_name: *`<br /> Fyll inn navnet på den nye .BIN-filen <br />
`path_to_images: *`<br /> Hvor på kontrollnoden nye imaget ligger<br />
`upgrade_ios_version: *`<br /> Versjonsnavn på det nye imaget <br />

På Cisco 3650 L3-svitsjene må disse variablene fylles inn 

`new_image_name: *`<br /> Fyll inn navnet på den nye .BIN-filen <br />
`path_to_images: *`<br /> Fyll inn navnet på den nye .BIN-filen <br />
`upgrade_ios_version: *`<br /> Versjonsnavn på det nye imaget<br />
`old_ios_version: *`<br /> Versjonsnavn på det gamle imaget <br />

### send_konfigfil-*
Brukes til å sende konfigurasjonsfiler til nettverksutstyret, variabler som må fylles inn før.

`mappe: *`<br /> Fyll mappenavnet på destinasjonen på nettverksutstyret <br />
`vei: *`<br /> Hvor mappen er på kontrollnoden <br />
`fil: *`<br /> Filnavn på filen som skal overføres<br />
`flash: *`<br /> Navn på flash på de administrerte nodene <br />



## Oversikt over maskiner i Cisco-laben
På Cisco-laben har vi ni stativer der nettverksutstyret er montert. Ett stativ regnes som en pod.
I filen maskinListe finnes en oversikt over alle disse maskinene. Denne listen er formatert som følger:

podnr-> skrevet som nummeret på poden + pd foran. Feks "pd8"
posisjonsnr-> skrevet som hvilken rad i poden maskinene ligger, telt ovenfra og nedover,  + ps. Ref. "ps2"
modell-> Modellen på maskinen. Skrevet uten mellomrom for at koden skal fungere. Ref. "CiscoCatalyst2960p"
MAC-adresser-> Liste med MAC-adresser som tilhører denne maskinen. Ref. "dc:eb:94:ad:4c:1a"

En tom linje indikerer at slutten på MAC-adresser er nådd, og det er mulig å begynne på en ny maskin.

maskinListeBackup er en backupliste i tilfelle maskinListe skulle bli korrupt. Dette er nødvendig ettersom MAC-adressene er lest av manuelt fra utstyret, og det vil ta unødvendig mye tid å gjøre dette dersom det kan unngås.

### Legg til nytt utsyr i oversikten over maskiner i laben
For å registrere at du har koblet til en ny maskin i en pod, gjør det følgende:
kjør scriptet macAdrBehandling.py
velg alternativ 1: "Legg til maskin"
*Det er ikke nødvendig å legge til ps/pd i podnr og posisjonnr*
Skriv inn podnr.
Skriv inn posisjonsnr:
Kopier eksisterende modellnavn og lim inn eller skriv ditt eget hvis det er en helt ny modell. Husk, ingen mellomrom.
Skriv inn MAC-adresser. En om gangen.
Skriv 'q' når ferdig

Da skal maskinen ha bli lagt til. Det er anbefalt å kopiere den nye maskinen fra maskinListe til maskinListeBackup manuelt hvis du ønsker at backupen skal ha denne dataen. 

### Fjerne en maskin i oversikten over maskiner i laben
For å registrere at du har fjernet en maskin fra en pod, gjør det følgende:
kjør scriptet macAdrBehandling.py
velg alternativ 2: "Slett maskin"
*Det er ikke nødvendig å legge til ps/pd i podnr og posisjonnr*
Skriv inn podnr.
Skriv inn posisjonsnr:

NB: Hvis det er to tomme linjer etter hverandre vil ikke nødvendigvis maskinen bli slettet. Da vil scriptet kun rette opp i mengden linjehopp. Er du usikker på om maskinen fremdeles er i lista kan du kjøre alternativ 3: "Sjekk om maskin finnes" eller bare kjøre "Slett maskin" en gang til.

Da skal maskinen ha bli fjernet. Det er anbefalt å slette den gamle maskinen fra maskinListeBackup manuelt hvis du ønsker at backupen skal registrere at denne maskien har blitt fjernet. Backupen er der i tilfelle noe går galt under endring av maskinListe, ettersom det tar lang tid å få tak i alle adressene i laben og organisere dem igjen.

### Sjekke at en maskin er registrert i maskinListe
For å sjekke at en maskin finnes i maskinListe, gjør det følgende:
kjør scriptet macAdrBehandling.py
velg alternativ 3: "Finnes maskin"
*Ikke legg til ps/pd i podnr og posisjonnr*
Skriv inn podnr.
Skriv inn posisjonsnr:

## Forklaring av enkelte skripter
Ved endring av prototypen eller for å få bedre forståelse av funksjonene, finnes det relevant informasjon under.

### madAdrBehandling.txt
Fil som brukes ved endring av oversikt over maskiner. Vær det å legge til, slette, eller se om maskinen finnes. For utdypning av hvordan å bruke dette scriptet, se i "Oversikt over maskiner i Cisco-laben" i manualen.

### ansiblePrep
Denne filen lager en fil som Ansible trenger når den skal skape en SSH-tilkobling til maskinene i laben.



## Feilsøking
---

Hvis *ansible-galaxy collection install cisco.ios* feiler kan disse kommandoene være til hjelp 
> sudo apt install python3-pip

> sudo -H pip install -Iv 'resolvelib<0.6.0'
> sudo apt install python3-scp
---
Hvis installasjonen av pip eller Scapy feiler, kan denne kommandoen være til hjelp

> sudo add-apt-repository universe
---

Hvis SCP-filer får feilmeldingen *Insert error message*, kan det være fordi Ansible prøver å bruke *ansible-pylibssh* som ikke fungerer med Cisco-utstyret, dette kan løses ved å avinstallere *ansible-pylibssh*.
>  pip uninstall ansible-pylibssh
---

* Relevant for konfigurasjon av nettverksutstyret på Cisco-laboratoriet ved NTNU i Gjøvik
