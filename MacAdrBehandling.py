from importlib.util import module_for_loader
from pickle import FALSE
from pkgutil import iter_modules
from unicodedata import name
import re


#Sjekker om en liste allerede har en duplikat av input variabelen
#Returnerer true hvis den har, false hvis ikke
def HarListeDuplikat(liste, input):
    for x in range(len(liste)):
        if liste[x] == input:
            return True
        else:
            return False

#Sjekker om input er skrevet som en gylidg mac-adresse. 
def ErStringMACAdr(input):
    #Definerer mac adr sitt mønster
    macAdrPattern = re.compile(r'(?:[0-9a-fA-F]:?){12}')
    if re.match(macAdrPattern,input):
        return True
    else:
        return False

#Sjekker om en spesifikk MAC-adresse i biblioteket er lik MAC-adresse i filen MAC.txt
def strCmp(MACadr, MACidentifier):
    if(MACadr == MACidentifier):
        return True
    else:
        return False

#Sjekker om alle MAC-adressene i biblioteket er lik noen av MAC-adressene i filen MAC.txt
def strCmpAlle(MACadr, MACidentifier):
    for item in MACidentifier():
        if(MACidentifier[item] == MACadr[item]):
            print(MACidentifier.index() + " " + MACidentifier + "\n")
            return MACidentifier
        else:
            return False

#Knytter MAC-adresse til IP med index og henter IP fra IP.txt
def hentIP(MACidentifier, IPidentifier):
    if(MACidentifier == True):
        indexNr = MACidentifier.index()
        return IPidentifier[indexNr]
    else:
        return False

#Legger til IP-adresse i maskinliste
def leggtilIP(MACidentifier, IPidentifier, MACadr):
    MACADR = MACbibliotek(MACidentifier)
    if(strCmpAlle(MACADR, MACadr)):
        fil = open("maskinliste", "a")
        for item in list():
            hentIP(MACidentifier, IPidentifier)
            if(len(item) == 12):
                if(item == ""):
                    fil.write(IPidentifier[item] +'\n')  
                    print(IPidentifier[item] + '\n')
    else: print("MAC-adressene matcher ikke")
      
#Går gjennom og leser listen med MAC-adresser hentet med Scapy
def MACbibliotek(MACidentifier):
    #Lager et filobjekt som åpner opp listen over MAC-adresser hentet med Scapy
    with open("MAC.txt", "r") as lesfil:
        for line in lesfil:
            MACidentifier(line.rstrip())
            if lesfil:
                return MACidentifier()
            if not lesfil:
                i = False
        lesfil.close()
        return False

#Går gjennom og leser listen med IP-adresser hentet med Scapy
def IPbibliotek(IPidentifier):
    #Lager et filobjekt som åpner opp listen over MAC-adresser hentet med Scapy
    fil = open("IP.txt", "r")
    #Looper gjennom filen
    i = True
    while i:
        #Leser linje for linje
        lesfil = fil.readline()
         #Dersom filen inneholder IPidentifier + linjeskift, returner True
        if lesfil == (IPidentifier) + '\n':
            return True
        #Slutten av filen?
        if not lesfil:
            i = False
        #Lukk fil
        fil.close()
        #Returnerer False dersom MAC-adresser ikke matcher
        return False

#Sjekker om Mac-adressen allerede eksisterer i maskinListe.txt
#Returnerer true hvis den finnes, false hvis ikke
def FinnesAdresse(MACadr):

    #Lager et filobjekt som lar deg åpne, lese i filen maskinListe
    fil = open("maskinListe","r")
    #Looper gjennom hele filen
    i = True
    while i:
        lesfil = fil.readline()
        #Hvis det er tomt linjebytte. (Markerer starten av et nytt Maskin-objekt)
        #TODO: rydd opp denne. Kan brukes i andre funksoner men trengs ikke her. Bruktes bare for å sjekke noen ting
        #if lesfil in ['\n', '\r\n']:
        #    print("Space")
        #Hvis vi finner en match til MAC-adressen, returner true
        if lesfil == (MACadr.strip())+'\n':
            return True
        #Sjekker om vi har nådd slutten på filen. 
        if not lesfil:
            i = False
    #lukker fil 
    fil.close()
    #Sier ifra at vi ikke fant matchende MAC-adr
    return False

#Sjekker om kombinasjonen av pod of posisjon allerede finnes i maskinListe.txt. 
#Returnerer true hvis den finnes, false hvis ikke
def FinnesMaskin(pod, posisjon):
    fil = open("maskinListe","r")
    
    #Variabler som begge må bli true for å returnere true
    podfinnes = False
    posisjonfinnes = False

    #Looper gjennom hele filen
    i = True
    while i:
        lesfil = fil.readline()
        #Hvis vi finner en match til pod eller posisjon, marker som true
        if lesfil == (pod)+'\n':
            podfinnes = True
        if lesfil == (posisjon)+'\n':
            posisjonfinnes = True
        #Sjekker om vi har nådd slutten på filen. 
        if not lesfil:
            i = False
    #lukker fil 
    fil.close()

    #Returnerer resultat. True hvis pod-posisjon kombo allerede finnes, false hvis ikke
    if (podfinnes and posisjonfinnes):
        return True
    else:
        return False

#Legger til ny ny maskin i maskinListe.txt
def LeggTilMaskin():

    #Sjekk at kombinasjonen av pod og posisjon ikke er brukt allerede
    i = True
    while i:
        pod = "pd" + (input("Hvilket Pod-nr?: "))
        posisjon = "ps" + (input("Hvilket radnr i pod?: "))
        #Hvis de prøver å legge til på en pod og posisjon som allerede finnes, gi feil og få dem til å gjøre dem på nytt
        if FinnesMaskin(pod, posisjon):
            print("Det er allerede en maskin på den posisjonen. Slett den eksisterende maskinen hvis du ønsker å legge til en ny maskin i pod:", pod ," på posisjon:", posisjon)
        else:
            i = False
    print("Eksisterende modeller er:")
    PrintModeller()
    modell = input("Case sensitiv. Modellnavn: ")

    #Lar bruker legge til så mange adresser de vil
    i = True
    MACadr = []
    while i:
        adr = input("MAC adresse, 1 om gangen. "'"q"'" for ferdig: ")
        #Hvis brukeren er ferdig, avslutt Mac-adr innlesning
        if adr == "q":
            #Hvis du har lagt til hvertfall en addresse kan du gå videre. Hvis ikke må du legge inn minst en addresse
            if (len(MACadr) > 0):
                i = False
            else:
                print("Du må legge til minst en adresse")
        
        #Hvis mac-adressen er feilformatert, gi feilmelding
        elif not (len(adr) == 17 and ErStringMACAdr(adr)):
            print("Ikke et gyldig MAC-adresseformat. Skal skrives i formatet ??:??:??:??:??:??")
        elif FinnesAdresse(adr):
            print("Denne MAC-addressen er allerede registrert på en annen maskin.")
        elif HarListeDuplikat(MACadr,adr):
            print("Du har allerede prøvd å legge til denne addressen")
        #Legg Mac-adr til i en liste
        else:
            MACadr.append(adr)
    #Så legger du inn alle variablene: pod, posisjon, modell, macadr[] i den rekkefølgen til filen maskinListe
    fil = open("maskinListe","a")
    fil.write('\n')  
    fil.write(pod +'\n') 
    fil.write(posisjon +'\n')
    fil.write(modell +'\n')
    #Loop for å gå igjennom lister
    for x in range(len(MACadr)):
        fil.write(MACadr[x]+'\n')           
    fil.close()
    print("La til " + modell + " på pod " + pod + " i posisjon " + posisjon)

#Sletter en maskin i maskinListe
def SlettMaskin(pod, pos):
    fil = open("maskinListe","r")
    #Liste med alt som skal beholdes
    lagretInnhold = []

    #Loop gjennom hele filen og legg inn alt som skal beholdes i lagretinnhold
    i = True
    while i:
        
        #Liste søm brukes for å oppbevare informasjonen til en maskin før vi bestemmer om det skal beholdes elles slettes
        tempOppbevaring = []
        #Hvis vi kommer til nytt innslag av en maskin, legg i tempOppbevaring og sjekk om den skal beholdes
            #Sjekk at det ikke er en tom linje før vi leser. Det kan tulle til lesning av fil

        podKey = fil.readline()
        tempOppbevaring.append(podKey)

        posisjonKey = fil.readline()
        tempOppbevaring.append(posisjonKey)

        modell = fil.readline()
        tempOppbevaring.append(modell)

        #leser in alle mac addressene og legger dem i tempOppbevaring.
        #Stanser når vi når linjeskift eller slutt på filen
        e = True
        while e:
            lesfil = fil.readline()
            if ErStringMACAdr((lesfil[0:17])):
                tempOppbevaring.append(lesfil)
            else:
                e = False

        #legger in '\'n'

        if lesfil:
            tempOppbevaring.append(lesfil)


        #Sjekker om maskinen er markert som slettes. er den det sier den ifra. ellers legges til i det som skal lagres
        #Hvis input = nr
        if ((podKey == (pod +'\n')) and (posisjonKey == (pos +'\n'))):
            print("Sletter: ",modell, " På pod: ", podKey, " plass: ",posisjonKey )
        #Hvis input = pd/ps i tilegg til nr
        elif ((podKey == ('pd' + pod +'\n')) and (posisjonKey == ('ps' + pos +'\n'))):
            print("Sletter: ",modell, " På pod: ", podKey, " plass: ",posisjonKey )
        else:
            lagretInnhold.extend(tempOppbevaring)

            #Sjekker om vi har nådd slutten på filen. 
        if not lesfil:
            i = False
    fil.close()

    #Overskrive den gamle filen med det som skal beholdes
    fil = open("maskinListe","w")
    linjeskift = False
    for x in range(len(lagretInnhold)):
        if not ((lagretInnhold[x] == ('\n' or '\n\t')) and linjeskift):
            fil.write(lagretInnhold[x]) 

        #Passer på at vi ikke skriver in to tomme linjer etter hverandre.¨
            #Da dette kan føre til leseproblemer
        if lagretInnhold[x] == ('\n' or '\n\t'):
            linjeskift = True
        else:
            linjeskift = False
    fil.close()

#Fjerner \n og \t på slutten av variabel
def ryddOppLinjeskift(input):
    input = input.replace('\n',"")
    input = input.replace('\t',"")
    return input

#Printer ut alle modelltyper som finnes is maskinliste
def PrintModeller():
    fil = open("maskinListe","r")
      #liste for å finne eksisterende modeller
    modellListe = []

    #Loop gjennom hele filen
    i = True
    while i:

        #Posisjon og pod, ikke relevant, må leses forbi
        podKey = fil.readline()
        posisjonKey = fil.readline()

        #Infoen vi faktis bruker her. Modellnavn
        modell = fil.readline()
        modell = ryddOppLinjeskift(modell)
        
        #Legger inn modellnavne som finnes i maskinListe
        eksisterer = False
        for x in modellListe:
            if x == modell:
                eksisterer = True
        if (eksisterer == False): 
            modellListe.append(modell)


        #looper oss forbi mac addresser
        #Stanser når vi når linjeskift eller slutt på filen
        e = True
        while e:
            lesfil = fil.readline()
            if not ErStringMACAdr((lesfil)):
               e = False

        #Sjekker om vi har nådd slutten på filen. 
        if not lesfil:
            i = False
    fil.close()
    print(modellListe)


#Main. Meny for endringer i maskinListe

#Så lenge ikke q(avslutt), la bruker bruke scriptet ved å loope menyen
fortsett = True
#Den sagnomsuste menyen
while fortsett:
    print('\n')
    print("1: Legg til maskin")
    print("2: Slett maskin")
    print("3: Sjekk om maskin finnes")
    print("q: avslutt")
    valg = input("Hva vil du gjøre?: ")

    if valg == '1':
        LeggTilMaskin()
    elif valg == '2':
        pod = input("Hviklen pod er den i?: ")
        pod = ryddOppLinjeskift(pod)
        pos = input("Hviklen posisjon skal slettes?: ")
        pos = ryddOppLinjeskift(pos)
        SlettMaskin(pod,pos)
    elif valg == '3':
        pod = 'pd' + input("Hviklen pod?: ")
        pos = 'ps' + input("Hviklen posisjon?: ")
        print('\n')
        if FinnesMaskin(pod, pos):
            print("Maskinen finnes")
        else:
            print("Maskinen finnes ikke")
    elif valg == 'q':
        fortsett = False
    else:
        print("----------------Ikke gyldig input")