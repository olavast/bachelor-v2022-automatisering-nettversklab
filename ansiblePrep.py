from ctypes import sizeof
from pickle import TRUE
from queue import Empty
import re
#Sjekker om input er skrevet som en gylidg MAC-adresse. 
def ErStringMACAdr(input):
    #Definerer mac adr sitt mønster
    macAdrPattern = re.compile(r'(?:[0-9a-fA-F]:?){12}')
    if re.match(macAdrPattern,input):
        return True
    else:
        return False

def FinnesAdresse(MACadr):

    #Lager et filobjekt som åpner og leser i filen maskinListe.txt
    fil = open("maskinListe","r")
    #Looper gjennom hele filen
    i = True
    while i:
        lesfil = fil.readline()
  
        #Hvis vi finner en match til MAC-adressen, returner true
        if lesfil == (MACadr.strip())+'\n':
            return True
        #Sjekker om vi har nådd slutten på filen. 
        if not lesfil:
            i = False
    #lukker fil 
    fil.close()
    #Sier ifra at vi ikke fant matchende MAC-adr
    return False


#Forbereder filen AnsibleIpList. 
#Lager liste over alle maskinene, modellvarianten og IP-adressen som assosiert med nettverksutstyret. 
def FilprepForAnsible():
    fil = open("maskinListe","r")
    
    #Liste for å registrere de forskjellige modellvariantene
    modellListe = []
    #Bibliotek for midlertidig oppbevaring av info som skal skriver under modeltyper.
    maskinInfoListe = []
    #Loop gjennom hele filen
    i = True
    while i:

        #Hvis vi kommer til nytt innslag av en maskin, legg i tempOppbevaring
        podKey = fil.readline()
        podKey = ryddOppLinjeskift(podKey)

        posisjonKey = fil.readline()
        posisjonKey = ryddOppLinjeskift(posisjonKey)

        modell = fil.readline()
        modell = ryddOppLinjeskift(modell)
        
        #Sjekker om denne modellen har blitt registrert før i funksjonen, hvis ikke legg den til i modellListe
        eksisterer = False
        for x in modellListe:
            if x == modell:
                eksisterer = True
        if (eksisterer == False): 
            modellListe.append(modell)
            maskinInfoListe.append([]) #Lager plass for den nye modellen


        #leser in alle MAC-adressene 
        #Stanser når vi når linjeskift eller slutt på filen
        ip =""
        e = True
        while e:
            lesfil = fil.readline()
            if ErStringMACAdr((lesfil)):
                if ErAdrInMACtxt(lesfil):
                    ip = HentIpFraMac(lesfil)
                    ip = ryddOppLinjeskift(ip)
            else:
                e = False

        navn = (podKey + posisjonKey + " ansible_host=" + ip)
        #Legger maskinen til i maskinInforListe riktig formatert med pod og posisjon kombinert.

        #Legger informasjonen om maskinen Ansible trenger til i maskinInfoListe
        #Først sjekker den hvilken modell maskinen er:
        teller = 0
        for i in modellListe:
            if i == modell:
                radNr = teller  
            teller += 1
        #Så legger den til maskinen i riktig rad
        teller = 0
        for i in maskinInfoListe:
            if teller == radNr:
                    i.append(navn)
            teller += 1

        #Sjekker om vi har nådd slutten på filen. 
        if not lesfil:
            i = False
    fil.close()





    #Skriver alt til filen
    fil = open("hosts.ini","w")

    #Skriver inn hver modell
    modellNr = 0
    bruktIp = 0
    for i in modellListe:
        fil.write('[' + i + ']\n')
        rad = 0

        #Skriver inn hver maskin som tilhører hver modell
        for y in maskinInfoListe:
            if modellNr == rad:
                for e in y: 
                    #Ta med modellen i filen hvis navn er lang nok til at den har med ip
                    if len(e) > 22:
                        fil.write(e + '\n') 
                        #Tell opp Ip-er som faktisk fikk tilkobling
                        bruktIp += 1
            rad += 1
        fil.write('\n')     #Legger til et linjeskift mellom hver modell
        modellNr +=1
    
    #Skriver variablene knytter til hver modell
    for i in modellListe:
        fil.write('[' + i + ':vars]\n')
        fil.write('ansible_connection=ansible.netcommon.network_cli\n')
        fil.write('ansible_network_os=cisco.ios.ios\n')
        fil.write('ansible_user=cisco\n')
        fil.write('ansible_password=cisco\n')
        fil.write('ansible_become=yes\n')
        fil.write('ansible_become_method=enable\n')
        fil.write('ansible_become_password=cisco\n')
        fil.write('\n')     #Legger til et linjeskift mellom hver modell


    #Gir statistikk om hvor mange ip addresser som finnes, og hvor mange vi klarte å koble til en maskin
    fil = open("IP","r")
    lesfil = fil.readline()
    totalIp = 1
    #Teller hvor mange IP-er vi har 
    while lesfil:
        lesfil = fil.readline()
        if len(lesfil) > 6:
            totalIp += 1
    print("Totalt antall IP-er:", totalIp)
    print("Antall tilkoblede IP-er:", bruktIp)
    fil.close()

#Sjekker at MAC-addressen fra input korresponderer med Mac-adr funnet med Scapy og lagt i MAC.txt. 
#Returnerer den korresponderernde IP-adressen lokalisert i IP.txt
def HentIpFraMac(input):
    input = ryddOppLinjeskift(input)

    fil = open("MAC","r")
    lesfil = "Temp"
    #Holder øye med hvilken linje vi er på
    teller = 1
    radNr = 0
    match = False

    #Leser gjennom fila til du finner en match 
    while lesfil: 
            lesfil = fil.readline()
            lesfil = ryddOppLinjeskift(lesfil)
            if (lesfil == input):
                radNr = teller
                match = True
            teller += 1
    fil.close()

    #Åpner listen over IP-adresser og returnerer IPen som sammensvarer med MAC-adressen fra samme linje i MAC.txt
    if match:
        fil = open("IP","r")
        for x in range(radNr):
            lesfil = fil.readline()
        fil.close()
        lesfil = ryddOppLinjeskift(lesfil)
        return lesfil
    else:
        return ""

#Sjekker om MAC-adressen fra input finnes i Scapy sin MAC.txt. 
def ErAdrInMACtxt(input):
    fil = open("MAC","r")
    lesfil = fil.readline()
    lesfil = ryddOppLinjeskift(lesfil)
    input = ryddOppLinjeskift(input)

    while lesfil:
        if  input == (lesfil):
            fil.close()
            return True
        lesfil = fil.readline()
        lesfil = ryddOppLinjeskift(lesfil)

    fil.close()
    return False

#Fjerner \n og \t på slutten av variabel
def ryddOppLinjeskift(input):
    input = input.replace('\n',"")
    input = input.replace('\t',"")
    return input

FilprepForAnsible()

