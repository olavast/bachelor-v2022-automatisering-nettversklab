#!/bin/bash

if test -f $1 && ! [ -z "$1" ]; then
	sudo python hentMacIp.py -t 10.10.0.0/24
	python ansiblePrep.py
	ansible-playbook $1
else
	echo "Playbook ble ikke funnet, vennligst spesifiser hvilken playbook som skal kjøres"
fi 



