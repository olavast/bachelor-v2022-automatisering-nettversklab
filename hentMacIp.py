import scapy.all as scapy
import argparse

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--target', dest='target', help='Target IP Address/Adresses')
    options = parser.parse_args()

    if not options.target:
        parser.error("[-] Spesifiser en IP adresse eller adresser, bruk --help for mer informasjon.")
    return options


def scan(ip):
    arp_req_frame = scapy.ARP(pdst = ip)

    broadcast_ether_frame = scapy.Ether(dst = "ff:ff:ff:ff:ff:ff")

    broadcast_ether_arp_req_frame = broadcast_ether_frame / arp_req_frame

    answered_list = scapy.srp(broadcast_ether_arp_req_frame, timeout = 1, verbose = False)[0]
    resultat = []
    for i in range(0,len(answered_list)):
        client_dict = {"ip" : answered_list[i][1].psrc, "mac" : answered_list[i][1].hwsrc}
        resultat.append(client_dict)


    return resultat

def display_result(resultat):
    print("-----------------\nIP-address\tMAC-address\n-------------------")

    with open('IP', 'w') as f:
    	for i in resultat:
        #print("{}".format(i["ip"]))
             f.write("{}\n".format(i["ip"]))

    with open('MAC', 'w') as a:
    	for i in resultat:
        #print("{}".format(i["mac"]))
            a.write("{}\n".format(i["mac"]))
        
    

options = get_args()
scanned_output = scan(options.target)

display_result(scanned_output)


#MAC address library
#networkAdd = addresses.splitlines()[1:]
#networkAdd = set(add.split(None,2)[1] for add in networkAdd if add.strip())
#with open(sys.argv[1]) as infile: knownAdd = set(line.strip() for line in infile)

#print("Disse adressene er i filen, men ikke på nettverket")
#for add in knownAdd - networkAdd:
#        print(add)

#for item in text.split():
#    if item.count(':') == 5:
#        print item

#print [item for item in text.split() if item.count(':') == 5] 
